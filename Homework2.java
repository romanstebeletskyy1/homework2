import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Homework2 extends Matrix {
    public static void main(String[] args) {
        Random random = new Random();
        Scanner scanner = new Scanner(System.in);

        System.out.println("All set. Get ready to rumble!\n");
        String[][] gameField = new String[6][6];

        for (String[] row : gameField) {
            Arrays.fill(row, "-");
        }

        for (int i = 0; i < gameField.length; i++) {
            gameField[i][0] = String.valueOf(i);
            gameField[0][i] = String.valueOf(i);
        }

        matrixOutput(gameField);
        int rowOrColumn = random.nextInt(2);
        int targetRow = random.nextInt(1, 6);
        int targetColumn = random.nextInt(1, 6);

        int targetRow2 = targetRow;
        int targetRow3 = targetRow;
        int targetColumn2 = targetColumn;
        int targetColumn3 = targetColumn;
        int row;
        int column;

        if (rowOrColumn == 0) {
            if (targetRow > 1 && targetRow < 5) {
                targetRow2 = targetRow + 1;
                targetRow3 = targetRow - 1;
            } else if (targetRow == 1) {
                targetRow2 = targetRow + 1;
                targetRow3 = targetRow + 2;
            } else {
                targetRow2 = targetRow - 1;
                targetRow3 = targetRow - 2;
            }

        } else {
            if (targetColumn > 1 && targetColumn < 5) {
                targetColumn2 = targetColumn + 1;
                targetColumn3 = targetColumn - 1;
            } else if (targetColumn == 1) {
                targetColumn2 = targetColumn + 1;
                targetColumn3 = targetColumn + 2;
            } else {
                targetColumn2 = targetColumn - 1;
                targetColumn3 = targetColumn - 2;
            }
        }

        String hit = "*";
        String winHit = "×";

        while (true) {
            while (true) {
                System.out.println("Choose row to shoot: ");
                if (!scanner.hasNextInt()) {
                    System.err.println("The number must be an integer!!!\n");
                    scanner.next();
                } else {
                    row = scanner.nextInt();
                    if (row > 5 || row < 1) {
                        System.err.println("The number must be in the range [1-5]: ");
                    } else {
                        break;
                    }
                }
            }
            while (true) {
                System.out.println("Choose column to shoot: ");
                if (!scanner.hasNextInt()) {
                    System.err.println("The number must be an integer!!!\n");
                    scanner.next();
                } else {
                    column = scanner.nextInt();
                    if (column > 5 || column < 1) {
                        System.err.println("The number must be in the range [1-5]: ");
                    } else {
                        break;
                    }
                }
            }

            if ((row == targetRow && column == targetColumn)
                    || (row == targetRow2 && column == targetColumn2)
                    || (row == targetRow3 && column == targetColumn3)) {
                gameField[row][column] = winHit;
                System.out.println();
                matrixOutput(gameField);

            } else {
                gameField[row][column] = hit;
                System.out.println();
                matrixOutput(gameField);
            }

            if (gameField[targetRow][targetColumn].equals(winHit)
                    && gameField[targetRow2][targetColumn2].equals(winHit)
                    && gameField[targetRow3][targetColumn3].equals(winHit)) {
                System.out.println("\nYou have won!");
                break;
            }
        }
    }
}

